import configparser
from flask import Flask, Response, render_template, redirect, url_for, request
import sqlite3
from twilio.twiml.voice_response import Play, VoiceResponse
from twilio.twiml.messaging_response import MessagingResponse
import random

config = configparser.ConfigParser()
config.read("config.ini")
twilio = config["twilio"]
instance = config["instance"]
app = Flask(__name__)

dbcon = sqlite3.connect(instance["db"])
dbcur = dbcon.cursor()
# "hardcoded" as in no proper user management/interface yet
uid = 361747
username = dbcur.execute("select username from users where uid=" + str(uid)).fetchone()[0]
dbcon.close()

@app.route("/answer_call", methods=['GET', 'POST'])
def answer_call():
	dbcon = sqlite3.connect(instance["db"])
	dbcur = dbcon.cursor()
	resp = VoiceResponse()
	resp.play(instance["upload_url"] + username + "/" + random.choice(dbcur.execute("select filename from sounds where uid=" + str(uid) + " and enabled=true").fetchall())[0])
	dbcon.close()
	return Response(response=str(resp), mimetype="text/xml")

@app.route("/answer_text", methods=['GET', 'POST'])
def answer_text():
	dbcon = sqlite3.connect(instance["db"])
	dbcur = dbcon.cursor()
	resp = MessagingResponse()
	resp.message(random.choice(dbcur.execute("select retort from fortunes where uid=" + str(uid) + " and enabled=true").fetchall())[0])
	dbcon.close()
	return Response(response=str(resp), mimetype="text/xml")

@app.route("/", methods=['GET'])
def config_page():
	dbcon = sqlite3.connect(instance["db"])
	dbcur = dbcon.cursor()
	sounds = dbcur.execute("select filename, enabled from sounds where uid=" + str(uid)).fetchall()
	fortunes = dbcur.execute("select retort, enabled from fortunes where uid=" + str(uid)).fetchall()
	dbcon.close()
	return render_template("config.html", phone=twilio['phone'], bool=bool, sounds=sounds, fortunes=fortunes, uid=uid)

@app.route("/toggle_call", methods=['POST'])
def toggle_call():
	dbcon = sqlite3.connect(instance["db"])
	dbcur = dbcon.cursor()
	dbcur.execute("update sounds set enabled=? where filename=? and uid=?", (request.form['enabled'], request.form['filename'], request.form['uid']))
	dbcon.commit()
	dbcon.close()
	return redirect(url_for("config_page"))

@app.route("/toggle_text", methods=['POST'])
def toggle_text():
	dbcon = sqlite3.connect(instance["db"])
	dbcur = dbcon.cursor()
	dbcur.execute("update fortunes set enabled=? where retort=? and uid=?", (request.form['enabled'], request.form['retort'], request.form['uid']))
	dbcon.commit()
	dbcon.close()
	return redirect(url_for("config_page"))
